const restify = require('restify'),
	fs 		= require('fs'),
	_ 		= require('lodash'),
	corsMiddleware = require('restify-cors-middleware');

const heroesMocks = _.cloneDeep(require('./mocks/heroes'));

const FAKE_AUTH_VALID_TOKEN = "1234";

const cors = corsMiddleware({
	origins: ['*']
});

const server = restify.createServer({
  name: 'mock-api-heroes',
  version: '1.0.0'
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.fullResponse());

server.pre(
	function crossOrigin(req,res,next){
	  res.header('Access-Control-Allow-Origin', '*');
	  res.header('Access-Control-Allow-Credentials', 'true');
	  res.header('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
	  res.header('Access-Control-Allow-Methods', 'GET, PUT, DELETE ,POST, PATCH, OPTIONS');
	  res.header('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
	  res.header('Access-Control-Max-Age', '1000');
	  res.header('Allow', 'GET');
	  return next();
	}
  );

function setHeaders(res){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Credentials', 'true');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
	res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE ,POST, PATCH, OPTIONS');
	res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
	res.setHeader('Access-Control-Max-Age', '1000');
}



function checkAuth(req){

	const authHeader = req.header('Authorization');

	return (!_.isNil(authHeader)&& authHeader === FAKE_AUTH_VALID_TOKEN);

}

server.get('/application/isAlive',  (req, res, next) => {

	setHeaders(res)
	
	res.send(200, true);
  
  	return next();
});

server.get('/superheroes',  (req, res, next) => {

	// if(!checkAuth(req)){
	// 	res.send(401);
	// 	return next();
	// } 
	// setHeaders(res)
	
	const heroes = heroesMocks.heroes.map(hero => {
		hero.imageSrc = '/images/' + hero['webscraperOrder'] + '-image.jpg'
		return hero;
	});

	res.send(200, heroes);
  
	return next();

});

server.get('/superheroes/:heroId',  (req, res, next) => {
	// if(!checkAuth(req)){
	// 	res.send(401);
	// 	return next();
	// } 

	// setHeaders(res)
	
	const heroId = req.params.heroId;
	const maybeHero = _.find( heroesMocks.heroes, h => h.idHero == heroId);

	(maybeHero)?
		res.send(200, maybeHero) :
		res.send(404, null);
  
		return next();
});

server.post('/superheroes', (req, res, next) => {
	const newHero = req.body;

	if(!newHero) {
		res.send(400, { 'message': 'No Hero found'});
		return next();
	}
	newHero.history = newHero.history || 'No history yet';
	newHero.alignment = newHero.alignment || 'good';

	const newId = _.max(heroesMocks.heroes.map(hero => hero.idHero)) + 1;

	newHero.idHero = newId;
	heroesMocks.heroes.push(newHero);

	res.send(200, );
	return next();
});

server.get('/images/:imagePath', (req, res, next) => {
	const imgPath = req.params.imagePath;
	try {
		const img = fs.readFileSync('./mocks/images/' + imgPath);
		res.writeHead(200, {'Content-Type': 'image/jpg' });
		res.end(img, 'binary');
	} catch(e) {
		const img = fs.readFileSync('./mocks/images/unknown.jpg');
		res.writeHead(200, {'Content-Type': 'image/jpg' });
		res.end(img, 'binary');
	}
});

server.post('/login', (req, res, next) => {
	const credentials = req.body;
	if(!credentials) {
		res.send(401, { message: 'Bad request' });
		
	} else if(!credentials.username || !credentials.password) {
		res.send(401, { message: 'Bad request' });
	}else if(credentials.username === 'formation' && credentials.password === 'formation') {
		res.send(200, { token: FAKE_AUTH_VALID_TOKEN });
	} else {
		res.send(401, { message: 'Bad request' });
	}
	return next();
});

server.listen(8448,  () => {
  console.log('%s listening at %s', server.name, server.url);
});